﻿using PatientManagerCommon.ViewModel;
using PatientManagerCommon.Database;
using PatientManagerCommon.MVVM;
using System.Collections.ObjectModel;
using PatientManager.View;
using System.Windows;

namespace PatientManager.ViewModel
{
    public class MainWindowViewModel : ViewModelBase
    {
        private ObservableCollection<Patient> patients;
        private Patient selectedPatient;
        private Medication selectedMedication;

        public MainWindowViewModel()
        {
            DatabaseManager.InitializeDatabase();

            patients = new ObservableCollection<Patient>(DatabaseManager.GetAllPatients());
        }

        public RelayCommand AddPatientCommand => new RelayCommand(param => AddPatient());
        public RelayCommand EditPatientCommand => new RelayCommand(param => EditPatient(), predicate => HasPatientSelected());
        public RelayCommand DeletePatientCommand => new RelayCommand(param => DeletePatient(), predicate => HasPatientSelected());
        public RelayCommand AddMedicationCommand => new RelayCommand(param => AddMedication(), predicate => HasPatientSelected());
        public RelayCommand DeleteMedicationCommand => new RelayCommand(param => DeleteMedication(), predicate => HasMedicationSelected());
        public RelayCommand DeactivateMedicationCommand => new RelayCommand(param => DeactivateMedication(), predicate => HasMedicationSelected());
 
        private void AddPatient()
        {
            AddPatientViewModel addPatientVM = new AddPatientViewModel();
            AddPatientView addPatientView = new AddPatientView
            {
                DataContext = addPatientVM,
                Owner = Application.Current.MainWindow
            };
            addPatientVM.OnRequestClose += (s, e) => addPatientView.Close();
            addPatientView.ShowDialog();

            if (addPatientVM.Create)
            {
                Patient patient = new Patient
                {
                    FirstName = addPatientVM.FirstName,
                    LastName = addPatientVM.LastName,
                    AccountNumber = addPatientVM.AccountNumber
                };

                if (patient.ExistsInDatabase())
                {
                    MessageBox.Show("A patient with that account number already exists. Please try again.");
                }
                else
                {
                    Patients.Add(patient);
                    patient.SaveInDatabase();
                }
            }
        }

        private void EditPatient()
        {
            EditPatientViewModel editPatientVM = new EditPatientViewModel();
            EditPatientView editPatientView = new EditPatientView
            {
                DataContext = editPatientVM,
                Owner = Application.Current.MainWindow
            };
            editPatientVM.OnRequestClose += (s, e) => editPatientView.Close();
            editPatientView.ShowDialog();

            Patient patient = new Patient
            {
                FirstName = editPatientVM.FirstName,
                LastName = editPatientVM.LastName,
                AccountNumber = editPatientVM.AccountNumber
            };

            if (patient.ExistsInDatabase() && SelectedPatient.AccountNumber == patient.AccountNumber)
            {
                SelectedPatient.FirstName = editPatientVM.FirstName;
                SelectedPatient.LastName = editPatientVM.LastName;
                SelectedPatient.AccountNumber = editPatientVM.AccountNumber;
                SelectedPatient.SaveChanges();
                Patients.Add(SelectedPatient);
                Patients.Remove(SelectedPatient);
            }
            else if (patient.ExistsInDatabase() && SelectedPatient.AccountNumber != patient.AccountNumber)
            {
                MessageBox.Show("A patient with that account number already exists. Please try again.");
            }
            else
            {
                SelectedPatient.FirstName = editPatientVM.FirstName;
                SelectedPatient.LastName = editPatientVM.LastName;
                SelectedPatient.AccountNumber = editPatientVM.AccountNumber;
                SelectedPatient.SaveChanges();
                Patients.Add(SelectedPatient);
                Patients.Remove(SelectedPatient);
            }
        }

        private bool HasPatientSelected()
        {
            return SelectedPatient != null;
        }

        private void DeletePatient()
        {
            SelectedPatient.RemoveFromDatabase();
            Patients.Remove(SelectedPatient);
        }

        private void AddMedication()
        {
            AddMedicationViewModel addMedicationVM = new AddMedicationViewModel();
            AddMedicationView addMedicationView = new AddMedicationView
            {
                DataContext = addMedicationVM,
                Owner = Application.Current.MainWindow
            };
            addMedicationVM.OnRequestClose += (s, e) => addMedicationView.Close();
            addMedicationView.ShowDialog();

            if (addMedicationVM.Create)
            {
                Medication medication = new Medication
                {
                    PatientID = SelectedPatient.PatientID,
                    Name = addMedicationVM.MedicationName,
                    Dosage = addMedicationVM.Dosage,
                    Treatment = "Active"
                };
                SelectedPatient.Medications.Add(medication);

                medication.SaveInDatabase();
            }
        }

        private bool HasMedicationSelected()
        {
            return SelectedMedication != null;
        }

        private void DeleteMedication()
        {
            SelectedMedication.RemoveFromDatabase();
            SelectedPatient.Medications.Remove(SelectedMedication);
        }

        private void DeactivateMedication()
        {
            SelectedMedication.DeactivateMedicationFromDatabase();
            SelectedMedication.Treatment = "Inactive";
        }

        public ObservableCollection<Patient> Patients
        {
            get => patients;
            set
            {
                if (patients == value) return;
                patients = value;
                OnPropertyChanged();
            }
        }

        public Patient SelectedPatient
        {
            get => selectedPatient;
            set
            {
                if (selectedPatient == value) return;
                selectedPatient = value;
                OnPropertyChanged();
            }
        }

        public Medication SelectedMedication
        {
            get => selectedMedication;
            set
            {
                if (selectedMedication == value) return;
                selectedMedication = value;
                OnPropertyChanged();
            }
        }
    }
}
