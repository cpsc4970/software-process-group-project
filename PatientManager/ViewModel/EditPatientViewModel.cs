﻿using PatientManagerCommon.MVVM;
using System;

namespace PatientManager.ViewModel
{
    public class EditPatientViewModel : ViewModelBase
    {
        private string firstName, lastName, accountNumber;

        public bool Save { get; set; }
        public event EventHandler OnRequestClose;

        public RelayCommand SaveEditsCommand => new RelayCommand(param => SaveEdits(), predicate => CanSaveEdits());
        public RelayCommand CancelCommand => new RelayCommand(param => Cancel());

        public string FirstName
        {
            get => firstName;
            set
            {
                if (firstName == value) return;
                firstName = value;
                OnPropertyChanged();
            }
        }

        public string LastName
        {
            get => lastName;
            set
            {
                if (lastName == value) return;
                lastName = value;
                OnPropertyChanged();
            }
        }

        public string AccountNumber
        {
            get => accountNumber;
            set
            {
                if (accountNumber == value) return;
                accountNumber = value;
                OnPropertyChanged();
            }
        }

        private void SaveEdits()
        {
            Save = true;
            OnRequestClose(this, new EventArgs());
        }

        private void Cancel()
        {
            OnRequestClose(this, new EventArgs());
        }

        private bool CanSaveEdits()
        {
            return !string.IsNullOrEmpty(FirstName)
                && !string.IsNullOrEmpty(LastName)
                && !string.IsNullOrEmpty(AccountNumber);
        }
    }
}
